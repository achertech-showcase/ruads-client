package org.ruads.client.dto;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.ruads.client.dto.Filter.Type.FullText;
import static org.ruads.client.dto.Filter.Type.Range;

public class CriterionTest {

    @Test
    public void thatCriteriumCreatedWithAllowedFilter() {
        Filter fullTextFilter = new Filter(FullText, "name", "text");
        new Criterion(Criterion.Type.Text, Collections.singletonList(fullTextFilter));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenIllegalFiltersPassedToConstructorThenException() {
        Filter fullTextFilter = new Filter(FullText, "name", "text");
        Filter rangeFilter = new Filter(Range, "range", 1L, 2L);
        new Criterion(Criterion.Type.Text, Arrays.asList(fullTextFilter, rangeFilter));
    }
}