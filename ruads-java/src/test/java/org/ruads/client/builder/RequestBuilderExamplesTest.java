package org.ruads.client.builder;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.ruads.client.dto.Criterion;
import org.ruads.client.dto.Dictionary;
import org.ruads.client.dto.ScanRequest;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Arrays;

import static java.lang.String.format;
import static org.junit.Assert.assertEquals;

/**
 * Each test case corresponds to example from API doc
 * <a href="https://ruads.org/assets/manual/api_30.06.17.pdf">api_30.06.17.pdf</a>
 */
public class RequestBuilderExamplesTest {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Test
    public void testExamplePrices() {
        ScanRequest request = new RequestBuilder()
                .criteria(Criterion.Type.Price)
                    .rangeFilter(1000L, 1500L)
                .buildScanRequest();

        assertEquals(example("pricesExample"), request);
    }

    @Test
    public void testExampleApartments() {
        Dictionary apartment = new Dictionary("560ed4e7e4b031fb2c47a9b6", "apartment", null);
        Dictionary moscow = new Dictionary("56ebcccd900e070be270f8b6", "moscow", null);

        ScanRequest request = new RequestBuilder()
                .criteria(Criterion.Type.Category)
                    .dictionaryContainsFilter(true, apartment)
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, moscow)
                .criteria(Criterion.Type.Price)
                    .rangeFilter(1_000_000L, 3_000_000L)
                .buildScanRequest();

        assertEquals(example("apartmentsExample"), request);
    }

    @Test
    public void testExampleOmsk() {
        Dictionary omskRegion = new Dictionary("56ebccc9900e070be270f729", "omskRegion", null);
        Dictionary omsk = new Dictionary("56ebccc9900e070be270f72a", "omsk", null);
        LocalDateTime publishedFromDate = LocalDateTime.of(2017, 4, 1, 0, 0);

        ScanRequest request = new RequestBuilder()
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, omskRegion)
                    .dictionaryContainsFilter(false, omsk)
                .criteria(Criterion.Type.Phone)
                    .phoneFilter(true)
                .criteria(Criterion.Type.Published)
                    .dateRangeFilter(publishedFromDate)
                .criteria(Criterion.Type.Type)
                    .individualTypeFilter(true)
                .buildScanRequest();

        assertEquals(example("omskExample"), request);
    }

    @Test
    public void testExampleAuto() {
        Dictionary daewooBrand = new Dictionary("5916165f739b1cb138bb9ebb", "daewoo", "auto");
        Dictionary kemerovo = new Dictionary("56ebccc9900e070be270f66a", "kemerovo", null);

        ScanRequest request = new RequestBuilder()
                .criteria(Criterion.Type.CarDetails)
                    .dictionaryFilter("brandId", daewooBrand)
                    .rangeFilter("mileage", null, 10_000L)
                .criteria(Criterion.Type.Location)
                    .dictionaryContainsFilter(true, kemerovo)
                .criteria(Criterion.Type.Price)
                    .rangeFilter("price", 80_000L, 150_000L)
                .buildScanRequest();

        assertEquals(example("autoExample"), request);
    }

    @Test
    public void testFullText() {
        ScanRequest request = new RequestBuilder()
                .criteria(Criterion.Type.Text)
                    .fullTextFilter(new RequestBuilder.FullTextFilterBuilder("fullText")
                        .containsAnyPhrase(Arrays.asList("text1","text2")))
                .buildScanRequest();

        assertEquals(example("fullTextExample"), request);
    }

    private ScanRequest example(String name) {
        try {
            String json = new String(Files.readAllBytes(
                    Paths.get(getClass().getResource(format("/examples/%s.json", name)).toURI()))
            );
            return OBJECT_MAPPER.readValue(json, ScanRequest.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        }
    }

}