package org.ruads.client.builder;

import org.ruads.client.dto.Criterion;
import org.ruads.client.dto.Filter;

import java.util.ArrayList;
import java.util.List;

class CriterionBuilder {
    private final Criterion.Type type;
    private final List<Filter> filters = new ArrayList<>();

    CriterionBuilder(Criterion.Type type) {
        this.type = type;
    }

    Criterion build() {
        return new Criterion(type, filters);
    }

    void addFilter(Filter filter) {
        filters.add(filter);
    }
}
