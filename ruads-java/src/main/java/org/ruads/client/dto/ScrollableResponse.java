package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ScrollableResponse {
    private final Statistics statistics;
    private final List<Document> documents;
    private final String scrollId;
    private final boolean hasNext;

    public ScrollableResponse(
            @JsonProperty("statistics") Statistics statistics,
            @JsonProperty("documents") List<Document> documents,
            @JsonProperty("scrollId") String scrollId,
            @JsonProperty("hasNext") boolean hasNext
    ) {
        this.statistics = statistics;
        this.documents = documents;
        this.scrollId = scrollId;
        this.hasNext = hasNext;
    }

    public Statistics getStatistics() {
        return statistics;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public String getScrollId() {
        return scrollId;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    @Override
    public String toString() {
        return "ScrollableResponse{" +
                "statistics=" + statistics +
                ", documents=" + documents +
                ", scrollId='" + scrollId + '\'' +
                ", hasNext=" + hasNext +
                '}';
    }
}
