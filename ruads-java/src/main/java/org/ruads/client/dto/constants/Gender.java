package org.ruads.client.dto.constants;

public enum Gender {
    Male,
    Female,
    Unknown;

    public String getFilterName() {
        return this.name().toLowerCase();
    }
}
