package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static java.lang.String.format;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Filter {
    private final String type;
    private final String name;
    private final List<String> values;
    private final String value;
    private final Long max;
    private final Long min;
    // full text fields
    private final List<String> doesNotContainPhrases;
    private final List<String> containsAnyPhrase;
    private final List<String> containsPhrases;
    private final List<String> containsWords;
    private final List<String> containsAnyWord;
    private final List<String> doesNotContainWords;

    @JsonCreator
    private Filter(
            @JsonProperty("type") String type,
            @JsonProperty("name") String name,
            @JsonProperty("values") List<String> values,
            @JsonProperty("value") String value,
            @JsonProperty("min") Long min,
            @JsonProperty("max") Long max,
            @JsonProperty("doesNotContainPhrases") List<String> doesNotContainPhrases,
            @JsonProperty("containsAnyPhrase") List<String> containsAnyPhrase,
            @JsonProperty("containsPhrases") List<String> containsPhrases,
            @JsonProperty("containsWords") List<String> containsWords,
            @JsonProperty("containsAnyWord") List<String> containsAnyWord,
            @JsonProperty("doesNotContainWords") List<String> doesNotContainWords
    ) {
        this.type = type;
        this.name = name;
        this.values = values;
        this.value = value;
        this.max = max;
        this.min = min;

        this.doesNotContainPhrases = doesNotContainPhrases;
        this.containsAnyPhrase = containsAnyPhrase;
        this.containsPhrases=containsPhrases;
        this.containsWords = containsWords;
        this.containsAnyWord = containsAnyWord;
        this.doesNotContainWords = doesNotContainWords;
    }

    public Filter(
            Type type,
            String name,
            Long min,
            Long max
    ) {
        this(type.getValue(), name, null, null, min, max, null, null, null, null, null, null);
    }

    public Filter(
            Type type,
            String name,
            List<String> values
    ) {
        this(type.getValue(), name, values, null, null, null, null, null, null, null, null, null);
    }

    public Filter(
            Type type,
            String name,
            String value
    ) {
        this(type.getValue(), name, null, value, null, null, null, null, null, null, null, null);
    }

    public Filter(
            Type type,
            String name,
            List<String> doesNotContainPhrases,
            List<String> containsAnyPhrase,
            List<String> containsPhases,
            List<String> containsWords,
            List<String> containsAnyWord,
            List<String> doesNotContainWords
    ) {
        this(
                type.getValue(),
                name,
                null,
                null,
                null,
                null,
                doesNotContainPhrases,
                containsAnyPhrase,
                containsPhases,
                containsWords,
                containsAnyWord,
                doesNotContainWords
        );
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public List<String> getValues() {
        return values;
    }

    public String getValue() {
        return value;
    }

    public Long getMax() {
        return max;
    }

    public Long getMin() {
        return min;
    }

    public List<String> getDoesNotContainPhrases() {
        return doesNotContainPhrases;
    }

    public List<String> getContainsAnyPhrase() {
        return containsAnyPhrase;
    }

    public List<String> getContainsPhrases() {
        return containsPhrases;
    }

    public List<String> getContainsWords() {
        return containsWords;
    }

    public List<String> getContainsAnyWord() {
        return containsAnyWord;
    }

    public List<String> getDoesNotContainWords() {
        return doesNotContainWords;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Filter filter = (Filter) o;

        if (!type.equals(filter.type)) return false;
        if (!name.equals(filter.name)) return false;
        if (values != null ? !values.equals(filter.values) : filter.values != null) return false;
        if (value != null ? !value.equals(filter.value) : filter.value != null) return false;
        if (max != null ? !max.equals(filter.max) : filter.max != null) return false;
        if (min != null ? !min.equals(filter.min) : filter.min != null) return false;
        if (doesNotContainPhrases != null ? !doesNotContainPhrases.equals(filter.doesNotContainPhrases) : filter.doesNotContainPhrases != null)
            return false;
        if (containsAnyPhrase != null ? !containsAnyPhrase.equals(filter.containsAnyPhrase) : filter.containsAnyPhrase != null)
            return false;
        if (containsPhrases != null ? !containsPhrases.equals(filter.containsPhrases) : filter.containsPhrases != null)
            return false;
        if (containsWords != null ? !containsWords.equals(filter.containsWords) : filter.containsWords != null)
            return false;
        if (containsAnyWord != null ? !containsAnyWord.equals(filter.containsAnyWord) : filter.containsAnyWord != null)
            return false;
        return doesNotContainWords != null ? doesNotContainWords.equals(filter.doesNotContainWords) : filter.doesNotContainWords == null;
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (values != null ? values.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (max != null ? max.hashCode() : 0);
        result = 31 * result + (min != null ? min.hashCode() : 0);
        result = 31 * result + (doesNotContainPhrases != null ? doesNotContainPhrases.hashCode() : 0);
        result = 31 * result + (containsAnyPhrase != null ? containsAnyPhrase.hashCode() : 0);
        result = 31 * result + (containsPhrases != null ? containsPhrases.hashCode() : 0);
        result = 31 * result + (containsWords != null ? containsWords.hashCode() : 0);
        result = 31 * result + (containsAnyWord != null ? containsAnyWord.hashCode() : 0);
        result = 31 * result + (doesNotContainWords != null ? doesNotContainWords.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", values=" + values +
                ", value='" + value + '\'' +
                ", max=" + max +
                ", min=" + min +
                ", doesNotContainPhrases=" + doesNotContainPhrases +
                ", containsAnyPhrase=" + containsAnyPhrase +
                ", containsPhrases=" + containsPhrases +
                ", containsWords=" + containsWords +
                ", containsAnyWord=" + containsAnyWord +
                ", doesNotContainWords=" + doesNotContainWords +
                '}';
    }

    public enum Type {
        FullText,
        Dictionary,
        Checkbox,
        StringMatch,
        Range;

        private final String value;

        Type() {
            this.value = format(".%sFilter", name());
        }

        public String getValue() {
            return value;
        }

        public static Type fromValue(String value) {
            return Arrays.stream(Type.values())
                    .filter(t -> t.value.equals(value)).findFirst()
                    .orElseThrow(() -> new IllegalArgumentException(format("no type with value %s", value)));
        }
    }
}