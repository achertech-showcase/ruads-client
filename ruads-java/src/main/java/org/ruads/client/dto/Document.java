package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Document {
    private final String id;
    private final String type;
    private final String published;
    private final String category;
    private final String location;
    private final Long price;
    private final Long phone;
    private final String operator;
    private final String contact;
    private final String address;
    private final String text;
    private final String description;
    private final String url;
    private final String gender;
    private final String geo;

    public Document(
            @JsonProperty("id") String id,
            @JsonProperty("type") String type,
            @JsonProperty("published") String published,
            @JsonProperty("category") String category,
            @JsonProperty("location") String location,
            @JsonProperty("price") Long price,
            @JsonProperty("phone") Long phone,
            @JsonProperty("operator") String operator,
            @JsonProperty("contact") String contact,
            @JsonProperty("address") String address,
            @JsonProperty("text") String text,
            @JsonProperty("description") String description,
            @JsonProperty("url") String url,
            @JsonProperty("gender") String gender,
            @JsonProperty("geo") String geo
    ) {
        this.id = id;
        this.type = type;
        this.published = published;
        this.category = category;
        this.location = location;
        this.price = price;
        this.phone = phone;
        this.operator = operator;
        this.contact = contact;
        this.address = address;
        this.text = text;
        this.description = description;
        this.url = url;
        this.gender = gender;
        this.geo = geo;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getPublished() {
        return published;
    }

    public String getCategory() {
        return category;
    }

    public String getLocation() {
        return location;
    }

    public Long getPrice() {
        return price;
    }

    public Long getPhone() {
        return phone;
    }

    public String getOperator() {
        return operator;
    }

    public String getContact() {
        return contact;
    }

    public String getAddress() {
        return address;
    }

    public String getText() {
        return text;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl() {
        return url;
    }

    public String getGender() {
        return gender;
    }

    public String getGeo() {
        return geo;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", published='" + published + '\'' +
                ", category='" + category + '\'' +
                ", location='" + location + '\'' +
                ", price=" + price +
                ", phone=" + phone +
                ", operator='" + operator + '\'' +
                ", contact='" + contact + '\'' +
                ", address='" + address + '\'' +
                ", text='" + text + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", gender='" + gender + '\'' +
                ", geo='" + geo + '\'' +
                '}';
    }
}
