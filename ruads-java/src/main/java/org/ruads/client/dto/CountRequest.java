package org.ruads.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountRequest {
    private final List<Criterion> criteria;
    private final Boolean countUniquePhones;

    @JsonCreator
    public CountRequest(
            @JsonProperty("criteria") List<Criterion> criteria,
            @JsonProperty("countUniquePhones") Boolean countUniquePhones
    ) {
        this.criteria = criteria;
        this.countUniquePhones = countUniquePhones;
    }

    public CountRequest(List<Criterion> criteria) {
        this(criteria, null);
    }

    public List<Criterion> getCriteria() {
        return criteria;
    }

    public Boolean getCountUniquePhones() {
        return countUniquePhones;
    }

    @Override
    public String toString() {
        return "CountRequest{" +
                "criteria=" + criteria +
                ", countUniquePhones=" + countUniquePhones +
                '}';
    }
}
