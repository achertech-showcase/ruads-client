package org.ruads.client;

import org.ruads.client.dto.*;

import java.util.List;

public interface RuadsApi {
    List<Dictionary> findDictionaries(String name);

    List<Dictionary> findDictionaries(String name, String query);

    ScrollableResponse scan(ScanRequest request);

    ScrollableResponse scroll(ScrollRequest request);

    CountResponse count(CountRequest request);
}
