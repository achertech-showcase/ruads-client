ruads.org java API client
================================

this is java implementation for https://ruads.org/ API, described in [API doc](https://ruads.org/assets/manual/api_30.06.17.pdf)

See ruads-spring-client project and RuadsApiImplIntegrationTest for example of how to use library.
To run integration test, you need to create application-secrets.properties in test resources and put your ruads-api key here
(see application-secrets-example.properties).

ruads-java is http client agnostic, use what you like. Only dependency is jackson-databind.